import logging

import hsla3k.writers

logger = logging.getLogger('hsla3k')


class Dispatcher:

    def __init__(self):
        self.load_writers()

    def load_writers(self):
        "Loads all configured writers"
        writers = hsla3k.config.default_writers
        self.writers = [getattr(hsla3k.writers, x)() for x in writers]

    def dispatch(self, event_name, match, log_line):
        logger.debug(f'Dispatching {event_name} ...')   # noqa

        groups = {}
        if match:
            groups = match.groupdict()

        for writers in self.writers:
            writers.load_event(event_name, groups, log_line)


dispatcher = Dispatcher()
