import logging
import os

from hsla3k.dispatcher import dispatcher
import hsla3k.config


logger = logging.getLogger('hsla3k')


class LogParser:
    log_name = ''

    def __init__(self, args):
        self.log_file = args.log_file
        if not self.log_file:
            self.log_file = self.get_default_log_file()

        self.max_lines = args.max_lines or 10 ** 6

        logger.debug('LogParser created with '
                     + f'log_file: {self.log_file}, max_lines: {self.max_lines}')       # noqa

        self.events_expressions_map = hsla3k.config.events_expressions_map

    def get_default_log_file(self):
        # logs_path = '/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/'     # noqa
        # logs_path = 'logs_archive/20190105/'
        # log_file = 'Zone.log'
        return os.path.join(hsla3k.config.logs_path, self.log_name)

    def parse_log(self):
        logger.info('Starting log parser...')

        lines_count = 0
        logger.info(f'Getting log lines from {self.log_file}')

        with open(self.log_file) as log_lines:
            for log_line in log_lines.readlines():
                lines_count += 1
                logger.debug(f'Processing line number {lines_count}')
                if self.max_lines < lines_count:
                    logger.info('Max lines reached')
                    break

                for event_name, expression in self.events_expressions_map:
                    match = expression.search(log_line)
                    if match is not None:
                        dispatcher.dispatch(event_name, match, log_line)

        dispatcher.dispatch('end', None, '')

        logger.info('Log parser ends')


class PowerLogParser(LogParser):
    log_name = 'Power.log'

    def __init__(self, args):
        super().__init__(args)

        self.events_expressions_map = hsla3k.config.power_events_expressions_map
