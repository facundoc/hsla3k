import datetime
import json
import logging
import os
import shutil
import time

import hsla3k.config


logger = logging.getLogger('hsla3k')


class GameValidationError(Exception):
    pass


class GamesManager:
    # TODO: usar una base de datos

    def load(self):
        "Loads the games from the file"

        games_file = hsla3k.config.games_file
        logger.debug(f'Opening games file {games_file}')
        try:
            with open(games_file) as json_data:
                stored_games = json.load(json_data)
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            stored_games = {}

        return stored_games

    def backup_games(self):
        games_file = hsla3k.config.games_file
        base_name, extension = os.path.splitext(games_file)
        suffix = time.time()
        backup_file = f'{base_name}_{suffix}{extension}'        # noqa

        logger.debug(f'Creating backup file {backup_file}')
        try:
            shutil.copy(games_file, backup_file)
        except FileNotFoundError:
            pass

    def save(self, games):
        self.load()

        data_updated = False
        for game in games:
            game_id = game.game_id
            new_data = game.dump_data()
            old_data = stored_games.get(game_id, '')
            if new_data != old_data:
                stored_games[game_id] = new_data
                data_updated = True

        if data_updated:
            if hsla3k.config.games_backup_on_update:
                self.backup_games()

            logger.debug(f'Saving games file {games_file}')
            with open(hsla3k.config.games_file, 'w') as json_data:
                json.dump(stored_games, json_data)


class Game:
    STATUS_NEW = 'n'
    STATUS_COMPLETE = 'c'

    def __init__(self, start_time=None, players=None):
        if start_time is None:
            start_time = datetime.datetime.now().strftime('%H:%M%S.%f')
        self.start_time = start_time

        self.players = players or []

        self.status = self.STATUS_NEW
        self.winner = None

    @property
    def game_id(self):
        players = '+'.join(self.players)
        return f'{self.start_time}+{players}'       # noqa

    def dump_data(self):
        data = {
                'game_id': self.game_id,
                'players': self.players,
                'status': self.status,
                'winner': self.winner,
        }
        return data

    def set_winner(self, player_name):
        for player in self.players:
            if player == player_name:
                self.winner = player_name

        if self.winner is None:
            raise GameValidationError('The player is not in the game')
