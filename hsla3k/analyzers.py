from hsla3k.models import GamesManager


class GamesAnalyzer:

    def get_games(self):
        games = GamesManager().load()
        return games

    def get_players(self, games):
        players = {}
        for game in games.values():
            for player_name in game['players']:
                player_data = players.setdefault(player_name, {'games_played': 0, 'games_won': 0})
                player_data['games_played'] += 1

                if game['winner'] == player_name:
                    player_data['games_won'] += 1

        for players_data in players.values():
            players_data['win_rate'] = players_data['games_won'] / players_data['games_played']

        return players

    def analyze(self, games=None):
        if games is None:
            games = self.get_games()

        players = self.get_players(games)

        results = {
                'Total games': len(games),
                'Players': players,
        }

        return results
