import re

# Parsers config
logs_path = 'logs_archive/20190105/'


base_line_re = r'D (?P<time>\d{2}:\d{2}:\d{2}\.\d+).*ProcessChanges.*'

players = (
        (r'Players',
            re.compile(base_line_re + r'tag=PLAYSTATE value=PLAYING.*entity=(?P<player_name>\w+)')),
)

jjevents_expressions_map = (
        # (r'Game', re.compile(base_line_re + r'game=id=(?P<game_id>\d+)')),
        # (r'Player', re.compile(base_line_re + r'players\[(?P<player_index>\d+)\]')),
        # (r'Tag', re.compile(base_line_re + r'tag=(?P<tag>\w+)\s+value=(?P<value>\w+)')),
        # (r'Running', re.compile(base_line_re + r'tag=(?P<tag>\w+)\s+value=RUNNING')),
        # (r'Game', re.compile(base_line_re + r'entity=GameEntity')),
        # (r'Entity', re.compile(base_line_re + r'entity=(?P<entity>\w+)')),
        (r'Players',
            re.compile(base_line_re + r'tag=PLAYSTATE value=PLAYING.*entity=(?P<player_name>\w+)')),
)


test = (
        (r'GameEntity',
         base_line_re + r'GameEntity.*tag=TURN(?P<d>.*)'),
)


events_expressions_map = [(name, re.compile(expression)) for name, expression in test]

power_line_prefix = r'D (?P<time>\d{2}:\d{2}:\d{2}\.\d+).*PowerTaskList.DebugPrintPower().*'

power = (
        # ('all', '(?P<all>.*)'),
        ('playstate',
            power_line_prefix
            + r'Entity=(?P<player_name>[\w#]+) '
            + r'tag=PLAYSTATE value=(?P<result>\w+)'),
        ('gameentity',
            power_line_prefix
            + r'Entity=GameEntity '
            + r'tag=(?P<tag>\w+) value=(?P<result>\w+)'),
)
power_events_expressions_map = [(name, re.compile(expression)) for name, expression in power]


# Dispatcher config

# Writers that should be loaded by default
default_writers = (
        'ConsoleWriter',
        'GameWriter',
)


# Writers configs

# Location of the file with the games read from the logs
games_file = 'hsla3k_games.json'

# If True, every update of the games creates a backup file
games_backup_on_update = True
