#!/usr/bin/env python3
"""HSLA3K cli
"""
import argparse
import logging
import sys

from hsla3k.analyzers import GamesAnalyzer
from hsla3k.log_parser import PowerLogParser


def analyze_data(args):
    results = GamesAnalyzer().analyze()
    print(results)


def parse_logs(args):
    PowerLogParser(args).parse_log()


def __main__(args):
    actions = {
            'analyze': analyze_data,
            'parse': parse_logs,
    }

    action = actions.get(args.action, None)
    if action is None:
        logger.error('Unrecognized action')
    else:
        action(args)


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Hearthstone Log Analyzer 3000')

    args_parser.add_argument('action', help='parse: parse Hearthstone logs')
    args_parser.add_argument('-l', '--log_file')
    args_parser.add_argument('-m', '--max_lines', type=int)
    args_parser.add_argument('--log-level', default='INFO')

    args = args_parser.parse_args()

    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    logger = logging.getLogger('hsla3k')
    logger.setLevel(getattr(logging, args.log_level))

    __main__(args)
