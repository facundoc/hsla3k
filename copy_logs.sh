#!/bin/bash

LOGS_PATH="/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/"

DEST_DIR="logs_archive/$(date +%Y%m%d%H%M)"

mkdir -p "$DEST_DIR"

cp "$LOGS_PATH"* "$DEST_DIR"
