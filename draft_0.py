import argparse
import logging
import os
import re
import sys


def dispatch(event_name, match, log_line):
    groups = match.groupdict()
    logger.info(f'{event_name}, {groups}')                                            # noqa


class LogParser:
    base_line_re = r'D (?P<time>\d{2}:\d{2}:\d{2}\.\d+) '
    events_expressions_map = (
            (r'Game ID', re.compile(base_line_re + r'.*game=id=(?P<game_id>\d+)')),
    )

    def __init__(self, args):
        self.log_file = args.log_file
        if not self.log_file:
            self.log_file = self.get_default_log_file()

        self.max_lines = args.max_lines or 10 ** 6

        logger.debug('LogParser created with '
                     + f'log_file: {self.log_file}, max_lines: {self.max_lines}')       # noqa

    def get_default_log_file(self):
        logs_path = '/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/'
        log_file = 'Zone.log'
        return os.path.join(logs_path, log_file)

    def parse_log(self):
        logger.info('Starting log parser...')

        lines_count = 0
        logger.info(f'Getting log lines from {self.log_file}')

        with open(self.log_file) as log_lines:
            for log_line in log_lines.readlines():
                lines_count += 1
                logger.debug(f'Processing line number {lines_count}')
                if self.max_lines < lines_count:
                    logger.info('Max lines reached')
                    break

                for event_name, expression in self.events_expressions_map:
                    match = expression.search(log_line)
                    if match is not None:
                        dispatch(event_name, match, log_line)

        logger.info('Log parser ends')


def __main__(args):
    LogParser(args).parse_log()


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='HSLA3K')

    args_parser.add_argument('-l', '--log_file')
    args_parser.add_argument('-m', '--max_lines', type=int)
    # TODO: args_parser.add_argument('-c', '--config-file')

    args = args_parser.parse_args()

    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    logger = logging.getLogger('hsla3k')
    logger.setLevel(logging.INFO)

    __main__(args)


# TODO: leer desde stdin si log_file == '-'
